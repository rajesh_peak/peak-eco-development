USE [PBLSHCODS]
GO

/****** Object:  Table [dbo].[PUBLSH_ADDRESS]    Script Date: 28/05/2016 18:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PUBLSH_ADDRESS](
	[SourceSystem] [nvarchar](255) NULL,
	[SourceSystemId] [nvarchar](255) NULL,
	[CanonicalId] [int] NULL,
	[AddressType] [nvarchar](255) NULL,
	[OwnerID] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[AddressLine1] [nvarchar](255) NULL,
	[AddressLine2] [nvarchar](255) NULL,
	[AddressLine3] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL,
	[CountryCode] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Mobile] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[IsDefaultAddress] [nvarchar](255) NULL,
	[CreatedDate] [nvarchar](255) NULL,
	[CreatedBy] [nvarchar](255) NULL,
	[UpdatedDate] [nvarchar](255) NULL,
	[UpdatedBy] [nvarchar](255) NULL,
	[PROCESSEDFLAG] [char](2) NULL,
	[Old_CountryCode] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


