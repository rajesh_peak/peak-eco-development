USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ScheduledAddressChange_STG]    Script Date: 07/06/2016 15:37:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ScheduledAddressChange_STG](
	[Action_ScheduledAddressChange] [varchar](50) NULL,
	[JournalReaderCode] [varchar](8) NULL,
	[ScheduledDate] [varchar](8) NULL,
	[OldReaderCode] [varchar](8) NULL,
	[OldCompanyCode] [varchar](8) NULL,
	[OldRecipAddressCode] [varchar](8) NULL,
	[NewReaderCode] [varchar](8) NULL,
	[NewCompanyCode] [varchar](8) NULL,
	[NewRecipAddressCode] [varchar](8) NULL,
	[OldFAOCode] [varchar](8) NULL,
	[OldAgentCode] [varchar](8) NULL,
	[OldPayeeAddressCode] [varchar](8) NULL,
	[NewFAOCode] [varchar](8) NULL,
	[NewAgentCode] [varchar](8) NULL,
	[NewPayeeAddressCode] [varchar](8) NULL,
	[RevertDate] [varchar](8) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


