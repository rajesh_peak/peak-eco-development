USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECADD1_STG]    Script Date: 13/05/2016 01:49:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECADD1_STG](
	[AddressCode] [varchar](8) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[Town] [varchar](30) NULL,
	[Country] [varchar](30) NULL,
	[CountryCode] [varchar](5) NULL,
	[PostCode] [varchar](18) NULL,
	[GeoCode] [varchar](8) NULL,
	[DirectSelectionCode] [varchar](8) NULL,
	[MSRegion] [varchar](2) NULL,
	[DateCreated] [varchar](8) NULL,
	[DateModified] [varchar](8) NULL,
	[AuditDateStamp] [varchar](8) NULL,
	[AuditTimeStamp] [varchar](8) NULL,
	[UserName] [varchar](12) NULL,
	[CompanyCode] [varchar](8) NULL,
	[Status] [varchar](5) NULL,
	[SourceID] [varchar](9) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


