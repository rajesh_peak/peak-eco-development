USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECJRC1_STG]    Script Date: 13/05/2016 01:51:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECJRC1_STG](
	[JOURNALREADERCODE] [varchar](8) NULL,
	[JOBTITLECODE] [varchar](5) NULL,
	[JOBTITLEDESC] [varchar](30) NULL,
	[COMPANYCODE] [varchar](8) NULL,
	[JOURNALCODE] [varchar](3) NULL,
	[READERCODE] [varchar](8) NULL,
	[RECIPADDRESSCODE] [varchar](8) NULL,
	[PAYEEADDRESSCODE] [varchar](8) NULL,
	[CONTRACTSTATUS] [varchar](1) NULL,
	[ECJRC1PRIVATE] [varchar](1) NULL,
	[READERCLASS] [char](1) NULL,
	[DESPATCHMETHOD] [varchar](5) NULL,
	[REGDate] [varchar](8) NULL,
	[AGENTCODE] [varchar](8) NULL,
	[ECJRC1SUBTYPE] [varchar](5) NULL,
	[ECJRC1STATUS] [varchar](5) NULL,
	[SUBSTART] [varchar](8) NULL,
	[SUBEND] [varchar](8) NULL,
	[COPIES] [varchar](7) NULL,
	[SUBISSUES] [varchar](7) NULL,
	[DATECREATED] [varchar](8) NULL,
	[THEIRREFERENCE] [varchar](15) NULL,
	[PAIDAMOUNT] [varchar](12) NULL,
	[PAIDBALANCE] [varchar](12) NULL,
	[TOTALBALANCE] [varchar](12) NULL,
	[ISSUEPRICE] [varchar](12) NULL,
	[MICROFILMREELNO] [varchar](8) NULL,
	[MICROFILMFRAMENO] [varchar](7) NULL,
	[CHARGECODE] [varchar](5) NULL,
	[PAYMENTMETHODCODE] [varchar](5) NULL,
	[FREECOPIES] [varchar](7) NULL,
	[NOOFISSUES] [varchar](7) NULL,
	[LASTISSUESENT] [varchar](8) NULL,
	[VATBALANCE] [varchar](12) NULL,
	[RENEWALCURRENCYCODE] [varchar](3) NULL,
	[SOLICITINGCURRENCYCODE] [varchar](3) NULL,
	[FAOCODE] [varchar](8) NULL,
	[COUNTRYCODE] [varchar](5) NULL,
	[ISSUESTOGO] [varchar](7) NULL,
	[STARTISSUECODE] [varchar](6) NULL,
	[ENDISSUECODE] [varchar](6) NULL,
	[UNPAIDISSUECOUNT] [varchar](7) NULL,
	[GRACEDISSUECOUNT] [varchar](7) NULL,
	[ISSUEVAT] [varchar](12) NULL,
	[RENEWALNUMBER] [varchar](2) NULL,
	[PROMOTIONCODE] [varchar](5) NULL,
	[TERMCODE] [varchar](5) NULL,
	[INITIALPROMCODE] [varchar](5) NULL,
	[ISSUECOUNT] [varchar](7) NULL,
	[CYCLECODE] [varchar](5) NULL,
	[HANDDELIVERYEXCLUSION] [char](1) NULL,
	[PRIVACYCODE] [varchar](5) NULL,
	[AUDITPERCENTAGE] [varchar](5) NULL,
	[RECIPCOMPANYCODE] [varchar](8) NULL,
	[OUTPUTDESPATCHMETHOD] [varchar](5) NULL,
	[RENEWED] [varchar](1) NULL,
	[ACTUALSTARTDate] [varchar](8) NULL,
	[ACTUALSTARTISSUECODE] [varchar](8) NULL,
	[READERTYPE] [varchar](1) NULL,
	[SOURCEIDLINK] [varchar](9) NULL,
	[RENEWALPROMCODE] [varchar](5) NULL,
	[CONTIGUOUSISSUECOUNT] [varchar](7) NULL,
	[SUBRATE] [varchar](12) NULL,
	[MARDEVFLAG] [varchar](1) NULL,
	[AGEDate] [varchar](8) NULL,
	[QUALIFDate] [varchar](8) NULL,
	[ORDERDATE] [varchar](8) NULL,
	[TAXAMOUNT] [varchar](12) NULL,
	[COMMENT2] [varchar](70) NULL,
	[ORIGINALSOURCEIDLINK] [varchar](9) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


