USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECExtra]    Script Date: 02/06/2016 14:36:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECExtra](
	[JOURNALREADERCODE] [varchar](8) NULL,
	[RENEWALNUMBER] [varchar](2) NULL,
	[UID_ECEXTRA] [varchar](14) NULL,
	[ABCPRINTBAND] [varchar](1) NULL,
	[ABCDIGITALBAND] [varchar](1) NULL,
	[CONVERSIONRATE] [varchar](11) NULL,
	[TAXAMOUNT] [varchar](11) NULL,
	[DEBTORSAMOUNT] [varchar](11) NULL,
	[CREDITDEFERREDREVENUEGROSS] [varchar](11) NULL,
	[CREDITDEFERREDREVENUENET] [varchar](11) NULL,
	[CREDITDEFERREDREVENUETAX] [varchar](11) NULL,
	[BANKSORTCODE] [varchar](6) NULL,
	[BANKACCOUNTNUMBER] [varchar](10) NULL,
	[BANKACCOUNTNAME] [varchar](18) NULL,
	[NEXTDDDATE] [varchar](8) NULL,
	[CREDITCARDURN] [varchar](20) NULL,
	[CREDITCARDEXPIRYDATE] [varchar](6) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


