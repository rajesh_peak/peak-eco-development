USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECRDR1]    Script Date: 13/05/2016 01:52:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECRDR1](
	[COMPANYCODE] [varchar](8) NULL,
	[READERCODE] [varchar](8) NULL,
	[INITIALS] [varchar](5) NULL,
	[SALUTATION] [varchar](5) NULL,
	[CHRISTIANNAME] [varchar](20) NULL,
	[SURNAME] [varchar](20) NULL,
	[NAME] [varchar](30) NULL,
	[JOBTITLECODE] [varchar](5) NULL,
	[JOBTITLEDESC] [varchar](30) NULL,
	[STATUS] [varchar](5) NULL,
	[DATECREATED] [varchar](8) NULL,
	[DATEMODIFIED] [varchar](8) NULL,
	[Field1] [varchar](8) NULL,
	[Field2] [varchar](8) NULL,
	[Field3] [varchar](8) NULL,
	[Field4] [varchar](8) NULL,
	[Field5] [varchar](8) NULL,
	[Field6] [varchar](8) NULL,
	[Field7] [varchar](8) NULL,
	[Field8] [varchar](8) NULL,
	[Field9] [varchar](8) NULL,
	[Field10] [varchar](8) NULL,
	[Field11] [varchar](8) NULL,
	[Field12] [varchar](8) NULL,
	[Field13] [varchar](8) NULL,
	[Field14] [varchar](8) NULL,
	[Field15] [varchar](8) NULL,
	[Field16] [varchar](8) NULL,
	[Field17] [varchar](8) NULL,
	[Field18] [varchar](8) NULL,
	[Field19] [varchar](8) NULL,
	[Field20] [varchar](8) NULL,
	[Field21] [varchar](8) NULL,
	[Field22] [varchar](8) NULL,
	[Field23] [varchar](8) NULL,
	[Field24] [varchar](8) NULL,
	[Field25] [varchar](8) NULL,
	[Field26] [varchar](8) NULL,
	[Field27] [varchar](8) NULL,
	[Field28] [varchar](8) NULL,
	[Field29] [varchar](8) NULL,
	[Field30] [varchar](8) NULL,
	[Field31] [varchar](8) NULL,
	[Field32] [varchar](8) NULL,
	[Field33] [varchar](8) NULL,
	[Field34] [varchar](8) NULL,
	[Field35] [varchar](8) NULL,
	[Field36] [varchar](8) NULL,
	[Field37] [varchar](8) NULL,
	[Field38] [varchar](8) NULL,
	[Field39] [varchar](8) NULL,
	[Field40] [varchar](8) NULL,
	[Field41] [varchar](8) NULL,
	[Field42] [varchar](8) NULL,
	[Field43] [varchar](8) NULL,
	[Field44] [varchar](8) NULL,
	[Field45] [varchar](8) NULL,
	[Field46] [varchar](8) NULL,
	[Field47] [varchar](8) NULL,
	[Field48] [varchar](8) NULL,
	[Field49] [varchar](8) NULL,
	[Field50] [varchar](8) NULL,
	[TPS] [varchar](8) NULL,
	[FPS] [varchar](8) NULL,
	[DATEOFBIRTH] [varchar](8) NULL,
	[ALLOWDIRECTMAIL] [varchar](1) NULL,
	[ADDRESSCODE] [varchar](8) NULL,
	[USERNAME] [varchar](12) NULL,
	[CUSTOMERVATNO] [varchar](20) NULL,
	[PRIVACYCODE] [varchar](5) NULL,
	[TELEPHONE] [varchar](20) NULL,
	[DEPARTMENT] [varchar](30) NULL,
	[FAXNUMBER] [varchar](20) NULL,
	[SOURCEID] [varchar](9) NULL,
	[UKVATEXEMPT] [char](1) NULL,
	[EMAILADDRESS] [varchar](60) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


