USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECPRH1]    Script Date: 10/05/2016 18:48:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECPRH1](
	[JournalReaderCode] [varchar](8) NULL,
	[RenewalNumber] [varchar](2) NULL,
	[RenewalDate] [varchar](8) NULL,
	[DateExpired] [varchar](8) NULL,
	[JournalCode] [varchar](3) NULL,
	[SubType] [varchar](5) NULL,
	[CountryCode] [varchar](5) NULL,
	[ReaderClass] [varchar](1) NULL,
	[PromotionCode] [varchar](5) NULL,
	[TermCode] [varchar](5) NULL,
	[SubRate] [varchar](12) NULL,
	[SubStart] [varchar](8) NULL,
	[PaymentMethodCode] [varchar](5) NULL,
	[PaidAmount] [varchar](12) NULL,
	[CurrencyCode] [varchar](3) NULL,
	[Copies] [varchar](7) NULL,
	[InitialPromCode] [varchar](5) NULL,
	[ReaderType] [varchar](1) NULL,
	[RenewalPromCode] [varchar](5) NULL,
	[ReaderCode] [varchar](8) NULL,
	[ContractStatus] [varchar](1) NULL,
	[DespatchMethod] [varchar](5) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


