
USE [QSSSTGCODS]
GO

CREATE TABLE [QSSSTGCODS].[dbo].[Region](
	[CountryCode] [varchar](5) NULL,
	[CountryName] [varchar](50) NULL,
	[RegionCode] [varchar](10) NULL,
	[RegionName] [varchar](50) NULL,
	[parentcode] [varchar](10) NULL,
	[countrycodeiso3] [varchar](5) NULL
) 
