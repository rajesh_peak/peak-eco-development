USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[Comments]    Script Date: 15/07/2016 20:22:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Comments](
	[JournalReaderCode] [varchar](8) NULL,
	[CommentDate] [varchar](11) NULL,
	[CommentLineNumber] [varchar](2) NULL,
	[CommentText] [varchar](500) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


