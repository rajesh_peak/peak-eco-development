USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECANSF_STG]    Script Date: 16/05/2016 08:21:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECANSF_STG](
	[entitytype] [varchar](1) NULL,
	[entitycode] [varchar](8) NULL,
	[datavalue] [varchar](30) NULL,
	[sourceid] [varchar](9) NULL,
	[qualifirevalue] [varchar](4) NULL,
	[subjectcode] [varchar](9) NULL,
	[sourcetype] [varchar](1) NULL,
	[answertype] [varchar](1) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


