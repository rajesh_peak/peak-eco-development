USE [PBLSHCODS]
GO

/****** Object:  Index [idx_subscription_SourceSystemSubscriberAccountID]    Script Date: 18/05/2016 23:49:20 ******/
CREATE NONCLUSTERED INDEX [idx_subscription_SourceSystemSubscriberAccountID] ON [dbo].[PUBLSH_SUBSCRIPTION]
(
	[SourceSystemSubscriberAccountID] ASC
)
