USE [PBLSHCODS]
GO

/****** Object:  Index [idx_address_ownerid]    Script Date: 13/05/2016 00:04:49 ******/
CREATE NONCLUSTERED INDEX [idx_address_ownerid] ON [dbo].[PUBLSH_ADDRESS]
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


