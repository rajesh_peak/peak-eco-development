USE [PBLSHCODS]
GO

/****** Object:  Index [idx_subscription_SourceSystemBillingAccountID]    Script Date: 18/05/2016 23:49:20 ******/
CREATE NONCLUSTERED INDEX [idx_subscription_SourceSystemBillingAccountID] ON [dbo].[PUBLSH_SUBSCRIPTION]
(
	[SourceSystemBillingAccountID] ASC
)
