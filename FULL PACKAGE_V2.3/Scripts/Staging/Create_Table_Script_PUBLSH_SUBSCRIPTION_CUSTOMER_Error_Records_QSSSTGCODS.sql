USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[PUBLSH_SUBSCRIPTION_CUSTOMER_Error_Records]    Script Date: 19/05/2016 14:14:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PUBLSH_SUBSCRIPTION_CUSTOMER_Error_Records](
	[sourceSystem] [nvarchar](255) NULL,
	[sourceSystemId] [nvarchar](255) NULL,
	[CanonicalId] [nvarchar](255) NULL,
	[SubscriptionNumber] [nvarchar](255) NULL,
	[SubscriptionRenewalSequenceNo] [nvarchar](255) NULL,
	[SourceSystemSubscriberAccountID] [nvarchar](255) NULL,
	[SourceSystemBillingAccountID] [nvarchar](255) NULL,
	[BillingAddressId] [nvarchar](255) NULL,
	[DeliveryAddressID] [nvarchar](255) NULL,
	[LegacyOrderID] [nvarchar](255) NULL,
	[OrderSource] [nvarchar](255) NULL,
	[SubscriptionStatus] [nvarchar](255) NULL,
	[PaymentStatus] [nvarchar](255) NULL,
	[OrderDate] [nvarchar](255) NULL,
	[StartDate] [nvarchar](255) NULL,
	[ExpiryDate] [nvarchar](255) NULL,
	[PaymentDate] [nvarchar](255) NULL,
	[CardType] [nvarchar](255) NULL,
	[NoOfCopies] [nvarchar](255) NULL,
	[NoofIssues] [nvarchar](255) NULL,
	[TermType] [nvarchar](255) NULL,
	[TermLength] [nvarchar](255) NULL,
	[ProductCode] [nvarchar](255) NULL,
	[LegacyProductCode] [nvarchar](255) NULL,
	[LegacyPromoCode] [nvarchar](255) NULL,
	[LegacyCampaignCode] [nvarchar](255) NULL,
	[RateCardItemCode] [nvarchar](255) NULL,
	[PriceZoneCode] [nvarchar](255) NULL,
	[RegionCode] [nvarchar](255) NULL,
	[PriceZoneCountryISO3Code] [nvarchar](255) NULL,
	[BillCurrCode] [nvarchar](255) NULL,
	[BaseCurrCode] [nvarchar](255) NULL,
	[OfferCode] [nvarchar](255) NULL,
	[PromoCode] [nvarchar](255) NULL,
	[CampaignCode] [nvarchar](255) NULL,
	[EntryPointCode] [nvarchar](255) NULL,
	[JourneyCode] [nvarchar](255) NULL,
	[MarketingChannelCode] [nvarchar](255) NULL,
	[ModelSourceCode] [nvarchar](255) NULL,
	[AgentPromoCode] [nvarchar](255) NULL,
	[NewstandPriceBillCurr] [nvarchar](255) NULL,
	[ListPriceBillCurr] [nvarchar](255) NULL,
	[ValueBillCurr] [nvarchar](255) NULL,
	[ValueBaseCurr] [nvarchar](255) NULL,
	[PaidAmountBillCurr] [nvarchar](255) NULL,
	[PaidAmountBaseCurr] [nvarchar](255) NULL,
	[BilltoBaseExhRate] [nvarchar](255) NULL,
	[DiscountAmountBillCurr] [nvarchar](255) NULL,
	[DiscountAmountBaseCurr] [nvarchar](255) NULL,
	[TaxValueBillCurr] [nvarchar](255) NULL,
	[TaxValueBaseCurr] [nvarchar](255) NULL,
	[CommValueBillCur] [nvarchar](255) NULL,
	[CommValueBaseCur] [nvarchar](255) NULL,
	[PaymentMethod] [nvarchar](255) NULL,
	[DeliveryMethod] [nvarchar](255) NULL,
	[ValueIncludesComm] [nvarchar](255) NULL,
	[RenewalMethod] [nvarchar](255) NULL,
	[CreditIssues] [nvarchar](255) NULL,
	[FollowUpTermType] [nvarchar](255) NULL,
	[FollowUpTermLength] [nvarchar](255) NULL,
	[FollowUpListPriceBillCurr] [nvarchar](255) NULL,
	[FollowUpOfferPriceBillCurr] [nvarchar](255) NULL,
	[FollowUpDiscountAmountBillCurr] [nvarchar](255) NULL,
	[RenewalEffortNo] [nvarchar](255) NULL,
	[ProductID] [nvarchar](255) NULL,
	[RateCardItemID] [nvarchar](255) NULL,
	[OfferID] [nvarchar](255) NULL,
	[CancelOnRenewal] [nvarchar](255) NULL,
	[CancellationDate] [nvarchar](255) NULL,
	[ProcessedFlag] [char](2) NULL,
	[Old_PriceZoneCountryISO3Code] [nvarchar](255) NULL,
	[Old_SubscriptionRenewalSequenceNo] [nvarchar](255) NULL,
	[Old_subscriptionnumber] [nvarchar](255) NULL,
	[Old_sourcesystemid] [nvarchar](255) NULL,
	[Routecode1] [nvarchar](255) NULL,
	[Routecode2] [nvarchar](255) NULL,
	[Dcroute] [nvarchar](255) NULL,
	[Legacycomment] [nvarchar](255) NULL,
	[AGENTCODE] [nvarchar](255) NULL,
	[TAXAMOUNT] [nvarchar](255) NULL,
	[CONTRACTSTATUS] [nvarchar](255) NULL,
	[OfferItemCode] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


