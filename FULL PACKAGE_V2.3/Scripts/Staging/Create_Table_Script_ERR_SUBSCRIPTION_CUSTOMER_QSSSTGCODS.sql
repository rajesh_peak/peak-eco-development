USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ERR_SUBSCRIPTION_CUSTOMER]    Script Date: 19/05/2016 14:17:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ERR_SUBSCRIPTION_CUSTOMER](
	[sourceSystemId] [nvarchar](255) NULL,
	[SubscriptionNumber] [nvarchar](255) NULL,
	[SubscriptionRenewalSequenceNo] [nvarchar](255) NULL,
	[SourceSystemSubscriberAccountID] [nvarchar](255) NULL,
	[SourceSystemBillingAccountID] [nvarchar](255) NULL,
	[PKGRunDate] [date] NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorDescription] [nvarchar](255) NULL,
	[RemovedDate] [date] NULL,
	[flag_Moved] [char](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ERR_SUBSCRIPTION_CUSTOMER] ADD  DEFAULT ('N') FOR [flag_Moved]
GO


