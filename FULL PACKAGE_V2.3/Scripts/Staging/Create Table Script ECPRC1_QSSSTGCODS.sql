USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECPRC1]    Script Date: 10/05/2016 18:50:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECPRC1](
	[PROMOTIONCODE] [varchar](5) NULL,
	[JOURNALCODE] [varchar](3) NULL,
	[PUBLISHERPROMOTIONCODE] [varchar](20) NULL,
	[CHARGECODE] [varchar](5) NULL,
	[RENEWALPROMOTIONCODE] [varchar](5) NULL,
	[TOTALMAILED] [varchar](7) NULL,
	[RECEIVEDTODATE] [varchar](7) NULL,
	[DATEMAILED] [varchar](8) NULL,
	[CYCLECODE] [varchar](5) NULL,
	[DESCRIPTION] [varchar](30) NULL,
	[LETTERCODE] [varchar](7) NULL,
	[ABCREPORT] [varchar](1) NULL,
	[BPAREPORT] [varchar](1) NULL,
	[BULKOFFER] [varchar](1) NULL,
	[ADDRESSCHARGECODE] [varchar](5) NULL,
	[CLOSINGDATE] [varchar](8) NULL,
	[FREEISSUES] [varchar](7) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


