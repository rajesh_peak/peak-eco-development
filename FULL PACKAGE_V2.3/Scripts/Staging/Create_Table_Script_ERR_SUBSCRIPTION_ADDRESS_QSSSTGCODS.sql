USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ERR_SUBSCRIPTION_ADDRESS]    Script Date: 19/05/2016 14:15:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ERR_SUBSCRIPTION_ADDRESS](
	[sourceSystemId] [nvarchar](255) NULL,
	[SubscriptionNumber] [nvarchar](255) NULL,
	[SubscriptionRenewalSequenceNo] [nvarchar](255) NULL,
	[DeliveryaddressID] [nvarchar](255) NULL,
	[BillingaddressID] [nvarchar](255) NULL,
	[PKGRunDate] [date] NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorDescription] [nvarchar](255) NULL,
	[RemovedDate] [date] NULL,
	[flag_moved] [char](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ERR_SUBSCRIPTION_ADDRESS] ADD  DEFAULT ('N') FOR [flag_moved]
GO


