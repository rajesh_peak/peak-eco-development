USE [QSSSTGCODS]
GO

/****** Object:  Index [idx_NonUniqueNCI_OfferItem_sub_journalreadercode_renewnumber]    Script Date: 28/05/2016 17:57:23 ******/
CREATE NONCLUSTERED INDEX [idx_NonUniqueNCI_OfferItem_sub_journalreadercode_renewnumber] ON [dbo].[OfferItem]
(
	[sub_journalreadercode_renewnumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


