USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ContactHistory]    Script Date: 07/06/2016 12:35:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ContactHistory](
	[ContactType] [varchar](50) NULL,
	[ContactMethod] [varchar](5) NULL,
	[ContactDate] [varchar](8) NULL,
	[JournalReaderCode] [varchar](8) NULL,
	[Note] [varchar](50) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL,
	[endate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


