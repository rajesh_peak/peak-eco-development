USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ECRTS1_STG]    Script Date: 10/05/2016 18:46:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECRTS1_STG](
	[JOURNALCODE] [varchar](3) NULL,
	[CHARGECODE] [varchar](5) NULL,
	[DESPATCHMETHOD] [varchar](5) NULL,
	[CURRENCYCODE] [varchar](5) NULL,
	[TERMCODE] [varchar](5) NULL,
	[EFFECTIVESTARTDATE] [varchar](8) NULL,
	[EFFECTIVEENDDATE] [varchar](8) NULL,
	[CHARGERATE01] [varchar](12) NULL,
	[CHARGERATE02] [varchar](12) NULL,
	[CHARGERATE03] [varchar](12) NULL,
	[CHARGERATE04] [varchar](12) NULL,
	[CHARGERATE05] [varchar](12) NULL,
	[CHARGERATE06] [varchar](12) NULL,
	[CHARGERATE07] [varchar](12) NULL,
	[CHARGERATE08] [varchar](12) NULL,
	[CHARGERATE09] [varchar](12) NULL,
	[CHARGERATE10] [varchar](12) NULL,
	[PERCENTAGEVAT] [varchar](5) NULL,
	[USUALNOISSUES] [varchar](7) NULL,
	[PERCENTAGEOFBAR01] [varchar](5) NULL,
	[PERCENTAGEOFBAR02] [varchar](5) NULL,
	[PERCENTAGEOFBAR03] [varchar](5) NULL,
	[PERCENTAGEOFBAR04] [varchar](5) NULL,
	[PERCENTAGEOFBAR05] [varchar](5) NULL,
	[PERCENTAGEOFBAR06] [varchar](5) NULL,
	[PERCENTAGEOFBAR07] [varchar](5) NULL,
	[PERCENTAGEOFBAR08] [varchar](5) NULL,
	[PERCENTAGEOFBAR09] [varchar](5) NULL,
	[PERCENTAGEOFBAR10] [varchar](5) NULL,
	[RATEBAND01] [varchar](7) NULL,
	[RATEBAND02] [varchar](7) NULL,
	[RATEBAND03] [varchar](7) NULL,
	[RATEBAND04] [varchar](7) NULL,
	[RATEBAND05] [varchar](7) NULL,
	[RATEBAND06] [varchar](7) NULL,
	[RATEBAND07] [varchar](7) NULL,
	[RATEBAND08] [varchar](7) NULL,
	[RATEBAND09] [varchar](7) NULL,
	[RATEBAND10] [varchar](7) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


