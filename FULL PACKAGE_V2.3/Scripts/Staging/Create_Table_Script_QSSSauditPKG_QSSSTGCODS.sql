USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[QSSSauditPKG]    Script Date: 13/05/2016 01:53:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[QSSSauditPKG](
	[PKGName] [varchar](50) NULL,
	[DestRowCount] [int] NULL,
	[StartTime] [datetimeoffset] NULL,
	[EndTime] [datetimeoffset] NULL,
	[ExecutionTime] [int] NULL,
	[UserName] [varchar](50) NULL,
	[pkgstatus] [varchar](30) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


