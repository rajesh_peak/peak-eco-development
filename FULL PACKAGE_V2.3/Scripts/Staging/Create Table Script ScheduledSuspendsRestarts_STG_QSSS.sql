USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[ScheduledSuspendsRestarts_STG]    Script Date: 07/06/2016 18:26:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ScheduledSuspendsRestarts_STG](
	[Action_ScheduledSuspendsRestarts] [varchar](30) NULL,
	[JournalReaderCode] [varchar](8) NULL,
	[ScheduledDate] [varchar](8) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


