USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[OfferItem]    Script Date: 28/05/2016 17:55:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OfferItem](
	[JOURNALCODE] [varchar](3) NULL,
	[JOURNALREADERCODE] [varchar](8) NULL,
	[RENEWALNUMBER] [varchar](2) NULL,
	[IsStudent] [varchar](1) NOT NULL,
	[Autorenewal] [varchar](3) NOT NULL,
	[DESPATCHMETHOD] [varchar](5) NULL,
	[CHARGECODE] [varchar](5) NULL,
	[SOLICITINGCURRENCYCODE] [varchar](3) NULL,
	[COUNTRYCODE] [varchar](5) NULL,
	[PROMOTIONCODE] [varchar](5) NULL,
	[TERMCODE] [varchar](5) NULL,
	[SUBRATE] [varchar](12) NULL,
	[BetOffer] [varchar](2) NOT NULL,
	[BestValue] [varchar](2) NOT NULL,
	[PUBLISHERPROMOTIONCODE] [varchar](20) NULL,
	[offeritem_sub] [varchar](255) NULL,
	[sub_journalreadercode_renewnumber] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


