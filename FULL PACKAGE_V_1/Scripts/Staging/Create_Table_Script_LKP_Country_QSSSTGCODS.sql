USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[LKP_Country]    Script Date: 13/05/2016 00:32:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LKP_Country](
	[countrycode_ECADD1] [nvarchar](255) NULL,
	[countryCode_ISO3_GoogleOthers] [nvarchar](255) NULL,
	[comment] [nvarchar](255) NULL
) ON [PRIMARY]

GO


