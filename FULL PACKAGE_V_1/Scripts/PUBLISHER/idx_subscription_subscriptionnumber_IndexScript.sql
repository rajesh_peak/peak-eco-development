USE [PBLSHCODS]
GO

/****** Object:  Index [idx_subscription_subscriptionnumber]    Script Date: 13/05/2016 00:13:02 ******/
CREATE NONCLUSTERED INDEX [idx_subscription_subscriptionnumber] ON [dbo].[PUBLSH_SUBSCRIPTION]
(
	[SubscriptionNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


