USE [PBLSHCODS]
GO

/****** Object:  Index [idx_customer_sourcesystemid]    Script Date: 13/05/2016 00:08:39 ******/
CREATE NONCLUSTERED INDEX [idx_customer_sourcesystemid] ON [dbo].[PUBLSH_CUSTOMER]
(
	[SourceSystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


