USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[PaymentHistory_STG]    Script Date: 02-06-2016 15:01:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PaymentHistory_STG](
	[JOURNALREADERCODE] [varchar](8) NULL,
	[JOURNALCODE] [varchar](3) NULL,
	[AUDITDATESTAMP] [varchar](8) NULL,
	[AUDITTIMESTAMP] [varchar](8) NULL,
	[BATCHNUMBER] [varchar](8) NULL,
	[PAYMENTAMOUNT] [varchar](12) NULL,
	[PAYMENTDATE] [varchar](8) NULL,
	[PAYMENTVAT] [varchar](12) NULL,
	[REASONCODE] [varchar](5) NULL,
	[CURRENCYCODE] [varchar](5) NULL,
	[USERNAME] [varchar](12) NULL,
	[PAYMENTMETHODCODE] [varchar](5) NULL,
	[CREDITCARDNO] [varchar](20) NULL,
	[AUTHORIZATIONCODE] [varchar](15) NULL,
	[INVOICECODE] [varchar](7) NULL,
	[FREETEXTPMTHST] [varchar](20) NULL,
	[REASONGROUPCODE] [varchar](5) NULL,
	[ACCOUNTCODE] [varchar](8) NULL,
	[PAYMENTCONVERSIONRATE] [varchar](13) NULL,
	[VATRATE] [varchar](5) NULL,
	[ADJUSTMENTAMOUNT] [varchar](12) NULL,
	[ADJUSTMENTVAT] [varchar](12) NULL,
	[TRANSFERREDTO] [varchar](8) NULL,
	[TRANSFERREDFROM] [varchar](8) NULL,
	[TRANSACTIONTYPE] [varchar](5) NULL,
	[BANKPAYMENTAMOUNT] [varchar](12) NULL,
	[BANKPAYMENTVAT] [varchar](12) NULL,
	[ACCPERIOD] [varchar](8) NULL,
	[PAYMENTNET] [varchar](12) NULL,
	[BANKPAYMENTNET] [varchar](12) NULL,
	[ECSTATUS] [varchar](1) NULL,
	[ECCOUNTRYCODE] [varchar](2) NULL,
	[CUSTOMERVATNO] [varchar](20) NULL,
	[RENEWALNUMBER] [varchar](2) NULL,
	[SOURCEID] [varchar](9) NULL,
	[EXPIRYDATE] [varchar](6) NULL,
	[DESTACCOUNTCODE] [varchar](10) NULL,
	[DESTACCOUNTNAME] [varchar](18) NULL,
	[SORTCODE] [varchar](8) NULL,
	[BANKCURRENCYCODE] [varchar](5) NULL,
	[DimensionCheckSum] [varbinary](8000) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[endt] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


