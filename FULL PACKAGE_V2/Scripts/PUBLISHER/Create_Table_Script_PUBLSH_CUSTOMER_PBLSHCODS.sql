USE [PBLSHCODS]
GO

/****** Object:  Table [dbo].[PUBLSH_CUSTOMER]    Script Date: 28/05/2016 18:01:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PUBLSH_CUSTOMER](
	[SourceSystem] [nvarchar](255) NULL,
	[SourceSystemId] [nvarchar](255) NULL,
	[CanonicalId] [int] NULL,
	[LegacyCRN] [nvarchar](255) NULL,
	[LegacyReaderCode] [nvarchar](255) NULL,
	[AccountNumber] [nvarchar](255) NULL,
	[DrupalUserID] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[CountryCode] [nvarchar](255) NULL,
	[BirthYear] [nvarchar](255) NULL,
	[Gender] [nvarchar](255) NULL,
	[Createddate] [date] NULL,
	[CreatedBy] [nvarchar](255) NULL,
	[Updatedate] [date] NULL,
	[UpdateBy] [nvarchar](255) NULL,
	[JobTitle] [nvarchar](255) NULL,
	[Industry] [nvarchar](255) NULL,
	[IndustrySICCode] [nvarchar](255) NULL,
	[UserType] [nvarchar](255) NULL,
	[AccountType] [nvarchar](255) NULL,
	[UserRegistrationdate] [date] NULL,
	[UserConversiondate] [date] NULL,
	[CompanyVatNumber] [nvarchar](255) NULL,
	[CompanyRegistrationNumber] [nvarchar](255) NULL,
	[AcquistionChannel] [nvarchar](255) NULL,
	[UserRegistrationOrigin] [nvarchar](255) NULL,
	[CompanyCode] [nvarchar](20) NULL,
	[UKVATExempt] [nvarchar](2) NULL,
	[DefaultSubDiscount] [nvarchar](255) NULL,
	[ParentCode] [nvarchar](10) NULL,
	[Load_Dt] [date] NULL,
	[LastUpdtDt] [date] NULL,
	[LastUpdtBy] [nvarchar](30) NULL,
	[ProcessedFlag] [char](2) NULL,
	[STATUS] [varchar](5) NULL,
	[Old_CountryCode] [nvarchar](255) NULL,
	[DEPARTMENT] [varchar](30) NULL,
	[Accountname] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


