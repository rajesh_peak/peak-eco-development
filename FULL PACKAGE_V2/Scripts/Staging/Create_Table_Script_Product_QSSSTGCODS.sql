USE [QSSSTGCODS]
GO

/****** Object:  Table [dbo].[Product]    Script Date: 18/05/2016 12:11:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Product](
	[Id] [varchar](5) NULL,
	[BundleCode] [varchar](5) NULL,
	[BundleName] [varchar](50) NULL,
	[BundleDesc] [varchar](50) NULL,
	[ABCRelevantFlag] [varchar](10) NULL,
	[ProductGroup] [varchar](30) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


